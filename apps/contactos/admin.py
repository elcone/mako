from django.contrib import admin
from .models import *

@admin.register(Contacto)
class ContactoAdmin(admin.ModelAdmin):
    list_display = [
        'apellidos',
        'nombre',
        'telefono',
        'fecha_de_nacimiento',
        'sexo',
        'propietario'
    ]

    list_filter = [
        'sexo',
        'propietario'
    ]

    def get_queryset(self, request):
        qs = super(ContactoAdmin, self).get_queryset(request)
        if not request.user.is_superuser:
            qs = qs.filter(propietario=request.user)
        return qs

    def save_model(self, request, obj, form, change):
        if not request.user.is_superuser:
            obj.propietario = request.user
        obj.save()

    def get_form(self, request, obj=None, **kwargs):
        form = super(ContactoAdmin,self).get_form(request, obj, **kwargs)
        if not request.user.is_superuser:
            form.base_fields['propietario'].queryset = form.base_fields['propietario'].queryset.filter(id=request.user.id)
        return form


@admin.register(Cita)
class CitaAdmin(admin.ModelAdmin):
    list_display = [
        'asunto',
        'inicio',
        'fin',
        'descripcion',
        'propietario'
    ]
