from rest_framework import serializers
from .models import *

class ContactoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contacto


class CitaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cita
