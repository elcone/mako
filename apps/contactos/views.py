from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from rest_framework.viewsets import ModelViewSet
from .models import *
from .serializers import *

class ContactoViewSet(ModelViewSet):
    serializer_class = ContactoSerializer
    queryset = Contacto.objects.all()

    def get_queryset(self):
        return Contacto.objects.filter(propietario=self.request.user)

    def perform_create(self, serializer):
        serializer.save(propietario=self.request.user)


class CitaViewSet(ModelViewSet):
    serializer_class = CitaSerializer
    queryset = Cita.objects.all()

    def get_queryset(self):
        return Cita.objects.filter(propietario=self.request.user)

    def perform_create(self, serializer):
        serializer.save(propietario=self.request.user)

@method_decorator(login_required, name='dispatch')
class ContactosTemplateView(TemplateView):
    template_name = 'contactos.html'
