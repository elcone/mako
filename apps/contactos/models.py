from django.db import models
from django.contrib.auth.models import User

class Contacto(models.Model):
    propietario = models.ForeignKey(User, related_name='contactos', null=True, blank=True)
    nombre = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    direccion = models.TextField(null=True, blank=True)
    telefono = models.CharField(max_length=20, null=True, blank=True)
    fecha_de_nacimiento = models.DateField(null=True, blank=True)

    MASCULINO, FEMENINO = 'M', 'F'
    SEXOS = (
        (MASCULINO, 'Masculino'),
        (FEMENINO, 'Femenino')
    )
    sexo = models.CharField(max_length=1,
        choices=SEXOS,
        default=MASCULINO)

    def _nombre_completo(self):
        return '%s %s' % (self.nombre, self.apellidos)
    nombre_completo = property(_nombre_completo)

    def __str__(self):
        return self.nombre_completo

    class Meta:
        verbose_name_plural = 'Contactos'


class Cita(models.Model):
    propietario = models.ForeignKey(User, related_name='citas', null=True, blank=True)
    contacto = models.ForeignKey(Contacto, related_name='citas', null=True, blank=True)
    asunto = models.CharField(max_length=50)
    inicio = models.DateTimeField()
    fin = models.DateTimeField()
    dia_entero = models.BooleanField(default=False)
    descripcion = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.asunto
