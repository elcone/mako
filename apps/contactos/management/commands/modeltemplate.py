from importlib import import_module
from django.core.management.base import BaseCommand, CommandError
from django.db import models

TEMPLATE = """
    <div class="form-group">
        <label class="col-lg-2 control-label">{CAMPO}</label>
        <div class="col-lg-8">
            {WIDGET}
        </div>
    </div>
"""

CHARFIELD_WIDGET = """<input type="text" id="{CAMPO}" name="{CAMPO}" placeholder="" class="form-control k-textbox" data-bind="value: {CAMPO}">"""
TEXTFIELD_WIDGET = """<textarea id="simple-textarea" name="{CAMPO}" class="form-control k-textbox" data-bind="value: {CAMPO}"></textarea>"""
DATEFIELD_WIDGET = """<input type="text" id="{CAMPO}" name="{CAMPO}" data-role="datepicker" data-bind="value: {CAMPO}">"""
DATETIMEFIELD_WIDGET = """<input type="text" id="{CAMPO}" name="{CAMPO}" data-role="datetimepicker" data-bind="value: {CAMPO}">"""
BOOLEANFIELD_WIDGET = """<input type="checkbox" id="{CAMPO}" name="{CAMPO}" class="k-checkbox" data-bind="checked: {CAMPO}">"""
FOREIGKEY_WIDGET = """<input id="{CAMPO}" name="{CAMPO}" data-role="dropdownlist" data-bind="value: {CAMPO}, source: FUENTE">"""
CHOICES_WIDGET = """<select id="{CAMPO}" name="{CAMPO}" data-role="dropdownlist" data-bind="value: {CAMPO}">{CHOICES}</select>"""

WIDGETS = {
    models.CharField.__name__: CHARFIELD_WIDGET,
    models.TextField.__name__: TEXTFIELD_WIDGET,
    models.DateField.__name__: DATEFIELD_WIDGET,
    models.DateTimeField.__name__: DATETIMEFIELD_WIDGET,
    models.BooleanField.__name__: BOOLEANFIELD_WIDGET,
    models.ForeignKey.__name__: FOREIGKEY_WIDGET,
    'CHOICES': CHOICES_WIDGET
}

class Command(BaseCommand):
    help = 'Crea un template de formulario para el modelo especificado'

    def add_arguments(self, parser):
        parser.add_argument('modelo', nargs='+', type=str)

    def handle(self, *args, **options):
        modelos = import_module('apps.contactos.models')
        modelo = getattr(modelos, options['modelo'][0])

        template = '<form class="form-horizontal">{CAMPOS}</form>'.replace('{CAMPOS}', self.crear_template(modelo))
        print(template)

    def crear_template(self, modelo):
        template = ''

        # https://code.djangoproject.com/wiki/new_meta_api
        for campo in modelo._meta.get_fields():
            template += self.template_del_campo(campo)

        return template

    def template_del_campo(self, campo):
        nombre = campo.name
        if not type(campo).__name__ in WIDGETS.keys():
            widget = WIDGETS[models.CharField.__name__]
        elif campo.flatchoices:
            widget = WIDGETS['CHOICES'].replace('{CHOICES}', self.opciones_del_campo(campo))
        else:
            widget = WIDGETS[type(campo).__name__]

        widget = TEMPLATE.replace('{WIDGET}', widget).replace('{CAMPO}', nombre)
        return widget

    def opciones_del_campo(self, campo):
        opciones = ['<option value="{0}">{1}</option>'.format(x[0], x[1]) for x in campo.flatchoices]
        return ''.join(opciones)
