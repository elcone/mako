from django.conf.urls import url, include
from django.contrib.auth import views

urlpatterns = [
    url('^$',
        views.login,
        { 'template_name': 'login.html' },
        name='usuarios_login'),
    url('^logout/$',
        views.logout_then_login,
        name='usuarios_logout')
]