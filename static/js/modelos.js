var Contacto = kendo.data.Model.define({
    id: "id",
    fields: {
        id: { editable: false, type: "number" },
        propietario: { type: "string" },
        nombre: { type: "string", validation: { required: true } },
        apellidos: { type: "string", validation: { required: true } },
        direccion: { type: "string" },
        telefono: { type: "string" },
        fecha_de_nacimiento: { type: "date" },
        sexo: { type: "string", defaultValue: "M" }
    }
});

var columnaModificarEliminar = {
    command: [{
        name: "edit",
        text: {
            edit: "Modificar",
            update: "Guardar",
            cancel: "Cancelar"
        }
    },
        { name: "destroy", text: "Borrar" }
    ], title: "&nbsp;", width: "200px"
};

var opcionesGrid = {
    height: 500,
    sortable: true,
    selectable: true,
    resizable: true,
    sortable: true
};

var opcionesCalendario = {
    //mobile: "phone",
    editable: true,
    allDaySlot: false,
    date: new Date(),
    //startTime: new Date("01/01/2014 08:00"),
    //endTime: new Date("01/01/2014 20:00"),
    workDayStart: new Date("01/01/2014 07:00"),
    workDayEnd: new Date("01/01/2014 21:00"),
    showWorkHours: true,
    height: 550,
    //footer: false,
    //timezone: "Etc/UTC",
    //workWeekEnd: 6,
    selectable: true,
    //eventTemplate: $("#template-evento").html(),
    views: [
        "day",
        { type: "week", selected: true },
        "month",
        "agenda"
    ],
    messages: {
        today: "Hoy",
        showFullDay: "Mostrar día completo",
        showWorkDay: "Mostrar horario de trabajo",
        views: {
            day: "Día",
            week: "Semana",
            workWeek: "Semana laboral",
            month: "Mes"
        }
    }
};

function crearTransport(url, crear, actualizar, borrar) {
    var transport = {
        parameterMap: function(model, operation) {
            if (operation !== "read" && model) {
                return kendo.stringify(model);
            }
        },
        read: url
    };
    if (crear) {
        transport.create = {
            url: url,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        }
    };
    if (actualizar) {
        transport.update = {
            url: function(modelo) {
                return url + modelo.id;
            },
            type: "PUT",
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        }
    };
    if (borrar) {
        transport.destroy = {
            url: function(modelo) {
                return url + modelo.id;
            },
            type: "DELETE",
            contentType: "application/json; charset=utf-8",
            dataType: "json"
        }
    };

    transport.parameterMap = function (model, operation) {
        if (operation !== "read" && model) {
            return kendo.stringify(model);
        }
    }

    return transport;
};